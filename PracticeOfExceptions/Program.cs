﻿using System;

namespace PracticeOfExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            void SomeMethod()
            {
                try
                {
                    AnotherMethod();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
                
            }
            void AnotherMethod()
            {
                throw new Exception("Вызвано исключение!");
            }

            SomeMethod();
        }
    }
}
